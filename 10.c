#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>


/**
 * Funkcja zwracająca logartym dziesiętny
 */
double log10(double x) {
    return log(x) / log(10.0);
}


/** 
 * Funkcja zwracająca wartość rozwiązania analitycznego dla danego x
 */
double exact(double x) {
    return (1.0 + x) * exp(-x);
}


/**
 * Metoda bezpośrednia Eulera
 */
double directEuler(int k, double yprev, double deltat) {
    if(k == 0) {
        return 1.0;
    }
    else {
        return yprev * (1.0 - deltat) + exp(-(k*deltat)) * deltat;
    }
}


/** 
 * Metoda pośrednia Eulera
 */
double indirectEuler(int k, double yprev, double deltat) {
    if(k == 0) {
        return 1.0;
    }
    else {
        return (deltat * exp(-(k+1) * deltat) + yprev) / (1.0 + deltat);
    }
}


/**
 * Metoda trapezów
 */
double trapezoid(int k, double yprev, double deltat) {
    if(k == 0) {
        return 1.0;
    }
    else {
        return (2.0 * yprev 
                - deltat * (yprev - exp(-(k*deltat)) - exp(-(k-1) * deltat)))
            / (2.0 + deltat);
    }
}


/**
 * Przygotowuje dane do wygenerowania wykresów rozwiązań równania różniczkowego
 */
void graphs(char *fileprefix, double (*f)(int, double, double),
        int k, double deltat) {
    int i;
    double yprev;
    char *filename = malloc(50 * sizeof(char));

    filename = strcpy(filename, fileprefix);
    filename = strcat(filename, "-graph.dat");
    FILE *file = fopen(filename, "w");

    for(i = 0; i < k; i++) {
        yprev = f(i, yprev, deltat);
        fprintf(file, "%20.16lf %20.16lf\n", 
                deltat * i, yprev);
    }

    fclose(file);

    free(filename);
}


/**
 * Przygotowyuje dane do wygenerowania wykresu zależności wartości bezwzględnej
 * maksymalnego błędu bezwzględnego od kroku sieci czasowej.
 */
double errors(double (*f)(int, double, double), double deltat) {
    int i;
    double yprev;
    double x = 0.0;
    double maxError = 0.0;
    double error;

    for(i = 0, x = 0.0; x < 0.2; i++, x+= deltat) {
        yprev = f(i, yprev, deltat);
        error = fabs(yprev - exact(x));
        if(maxError < error) {
            maxError = error;
        }
    }

    return maxError;
}


/**
 * Program główny
 */
int main() {

    graphs("direct_euler_unstable", directEuler, 49, 2.05);
    graphs("direct_euler", directEuler, 200, 0.5);
    graphs("indirect_euler", indirectEuler, 100, 0.1);
    graphs("trapezoid", trapezoid, 100, 0.1);


    FILE *file1 = fopen("direct_euler-errors.dat", "w");
    FILE *file2 = fopen("indirect_euler-errors.dat", "w");
    FILE *file3 = fopen("trapezoid-errors.dat", "w");

    double deltat;
    for(deltat = 0.0099; deltat > 0.000000001; deltat /= 1.05) {
        fprintf(file1,"%20.16lf %20.16lf\n", deltat, 
                errors(directEuler, deltat));
        fprintf(file2,"%20.16lf %20.16lf\n", deltat, 
                errors(indirectEuler, deltat));
        fprintf(file3,"%20.16lf %20.16lf\n", deltat, 
                errors(trapezoid, deltat));
        printf(".");
        fflush(stdout);
    }
    printf("\n");

    printf("Rzędy dokładności:\n");
    double order;
    order = 
        (log10(errors(directEuler, 1e-3)) - log10(errors(directEuler, 1e-4)))
        / (log10(1e-3) - log10(1e-4));
    printf("Metoda bezpośrednia Eulera: %lf\n", order);
    order = 
        (log10(errors(indirectEuler, 1e-3)) - log10(errors(indirectEuler, 1e-4)))
        / (log10(1e-3) - log10(1e-4));
    printf("Metoda pośrednia Eulera: %lf\n", order);
    order = 
        (log10(errors(trapezoid, 1e-3)) - log10(errors(trapezoid, 1e-4)))
        / (log10(1e-3) - log10(1e-4));
    printf("Metoda trapezów: %lf\n", order);

    fclose(file1);
    fclose(file2);
    fclose(file3);

    return 0;
}
