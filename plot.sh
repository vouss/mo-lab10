#!/bin/bash

graphfile=`mktemp`
cat >$graphfile <<EOF
set terminal wxt size 1266,668
set title "Wykresy rozwiązań dla metody bezpośredniej Eulera w warunkach numerycznej stabilności"
set xlabel "t"
set ylabel "y(t)"
set grid
show grid
y(x) = (1 + x) * exp(-x)
plot \
 y(x) with lines title "Rozwiązanie analityczne", \
 "direct_euler-graph.dat" using 1:2 with points title "Rozwiązanie numeryczne"
pause -1
EOF
$(gnuplot $graphfile &>/dev/null) &


graphfile=`mktemp`
cat >$graphfile <<EOF
set terminal wxt size 1266,668
set title "Wykresy rozwiązań dla metody bezpośredniej Eulera w warunkach numerycznej niestabilności"
set xlabel "t"
set ylabel "y(t)"
set grid
show grid
y(x) = (1 + x) * exp(-x)
plot \
 y(x) with lines title "Rozwiązanie analityczne", \
 "direct_euler_unstable-graph.dat" using 1:2 with points title "Rozwiązanie numeryczne"
pause -1
EOF
$(gnuplot $graphfile &>/dev/null) &


graphfile=`mktemp`
cat >$graphfile <<EOF
set terminal wxt size 1266,668
set title "Wykresy rozwiązań dla metody pośredniej Eulera"
set xlabel "t"
set ylabel "y(t)"
set grid
show grid
y(x) = (1 + x) * exp(-x)
plot \
 y(x) with lines title "Rozwiązanie analityczne", \
 "indirect_euler-graph.dat" using 1:2 with points title "Rozwiązanie numeryczne"
pause -1
EOF
$(gnuplot $graphfile &>/dev/null) &


graphfile=`mktemp`
cat >$graphfile <<EOF
set terminal wxt size 1266,668
set title "Wykresy rozwiązań dla metody trapezów"
set xlabel "t"
set ylabel "y(t)"
set grid
show grid
y(x) = (1 + x) * exp(-x)
plot \
 y(x) with lines title "Rozwiązanie analityczne", \
 "trapezoid-graph.dat" using 1:2 with points title "Rozwiązanie numeryczne"
pause -1
EOF
$(gnuplot $graphfile &>/dev/null) &


graphfile=`mktemp`
cat >$graphfile <<EOF
set logscale xy
set terminal wxt size 1266,668
set title "Zależności maksymalnych błędów bezwzględnych od kroku sieci czasowej"
set xlabel "dt"
set ylabel "maksymalny błąd bezwzględny"
set grid
show grid
y(x) = (1 + x) * exp(-x)
plot \
 "direct_euler-errors.dat" using 1:2 with lines title "Metoda bezpośrednia Eulera", \
 "indirect_euler-errors.dat" using 1:2 with lines title "Metoda pośrednia Eulera", \
 "trapezoid-errors.dat" using 1:2 with lines title "Metoda trapezów"
pause -1
EOF
$(gnuplot $graphfile &>/dev/null) &
